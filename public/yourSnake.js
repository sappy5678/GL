
var canvas;
var gl;
var bufferId;
var vPosition;
var cBuffer;
// var colors = [
    // vec4( 0.0, 0.0, 1.0, 1.0 ),  // blue
    // vec4( 0.0, 1.0, 1.0, 1.0 ),  // cyan
    // vec4( 1.0, 0.0, 1.0, 1.0 ),  // magenta
    // vec4( 0.0, 1.0, 1.0, 1.0 )   // cyan
// ];
var colors = vec4( 0.0, 0.0, 1.0, 1.0 );
var vertices = [
    vec2( -0.05, -0.05 ),
    vec2( -0.05,  0.05 ),
    vec2(  0.05,  0.05 ),
    vec2(  0.05, -0.05 )
];

var food_vertices = [
    vec2(0,0.05),
    vec2(-0.05,-0.05),
    vec2(0.05,-0.05)
];
var snake_bodies = [{x:0,y:0}];
var foods = [];

window.onload = function init()
{
    canvas = document.getElementById( "gl-canvas" );
    
    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    //  Configure WebGL
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.0, 0.0, 0.0, 1.0 );

    //Do something ...
    /////////////////////////////////////////////////////////////////////////////////
    // Load the cube location data into the GPU
    var program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    bufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER,bufferId);
    gl.bufferData( gl.ARRAY_BUFFER,flatten(vertices),gl.STATIC_DRAW);

    // Associate out shader variables with our data buffer
    vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    // Color cube
    cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten([colors,colors,colors,colors]), gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );

    locationLoc = gl.getUniformLocation(program,"location");


    canvas.addEventListener("click",function (event) {
        var t = vec2(-1 + 2*event.clientX/canvas.width,
            -1 + 2*(canvas.height-event.clientY)/canvas.height);
        t[0] = Math.round(t[0]*10);
        t[1] = Math.round(t[1]*10);

        if( !is_on_something(t[0],t[1],foods) &&
            !is_on_something(t[0],t[1],snake_bodies) &&
            !(Math.abs(t[0])===10 || Math.abs(t[1])===10)
          )
        {
            foods.push({x: t[0], y: t[1],color:vec4(Math.random(),Math.random(),Math.random(),1.0)});
            console.log(foods[0].color );
        }
    });
    //////////////////////////////////////////////////////////////////////////////////
    window.onkeydown = function( event ) {
        var key = String.fromCharCode(event.keyCode);

        switch( key ) {
            // up
            case 'W':
                snake_bodies.unshift({x:snake_bodies[0].x,y:snake_bodies[0].y+1});
                break;

            // down
            case 'S':
                snake_bodies.unshift({x:snake_bodies[0].x,y:snake_bodies[0].y-1});
                break;

            // left
            case 'A':
                snake_bodies.unshift({x:snake_bodies[0].x-1,y:snake_bodies[0].y});
                break;

            // right
            case 'D':
                snake_bodies.unshift({x:snake_bodies[0].x+1,y:snake_bodies[0].y});
                break;
        }

        // if collision
        if(is_collision(snake_bodies))
        {
            snake_bodies.shift();
        }

        // if eat food
        else if(is_eat_food(snake_bodies,foods))
        {
            // console.log("Eat");

            var food = remove_food(foods,snake_bodies[0].x,snake_bodies[0].y);
            colors = food.color;
        }

        // nothing happened
        else
        {
            snake_bodies.pop();
        }
    };

    render();
};

function is_on_something(x,y,something) {
    for(var i = 0; i < something.length; ++i)
    {
        if(x === something[i].x && y === something[i].y)
        {
            return true;
        }
    }

    return false;
}

function is_eat_food(snake,foods) {
    for(var i = 0; i < foods.length; ++i)
    {
        if(snake[0].x === foods[i].x && snake[0].y === foods[i].y)
        {
            return true;
        }
    }

    return false;
}

function is_collision(snake) {
    console.log(snake[0]);
    if(Math.abs(snake[0].x) === 10 || Math.abs(snake[0].y) === 10)
    {
        return true;
    }
    for(var i = 1; i < snake_bodies.length; ++i)
    {
        if(snake[0].x === snake[i].x && snake[0].y === snake[i].y)
        {
            return true;
        }
    }

    return false;
}

function remove_food(foods,x,y) {
    var tmp;
    for(var i = 0; i < foods.length; ++i)
    {
        if(foods[i].x === x && foods[i].y === y)
        {
            tmp = foods[i];
            foods.splice(i, 1);
        }
    }
    return tmp;

}

function render() {
    gl.clear( gl.COLOR_BUFFER_BIT );

    //------------------------------ Draw Snake -------------------------------------//
    gl.bindBuffer( gl.ARRAY_BUFFER,bufferId);
    gl.bufferData( gl.ARRAY_BUFFER,flatten(vertices),gl.STATIC_DRAW);
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten([colors,colors,colors,colors]), gl.STATIC_DRAW );
    for(var i = 0; i < snake_bodies.length; ++i)
    {
        gl.uniform2fv(locationLoc, vec2(snake_bodies[i].x/10, snake_bodies[i].y/10));
        gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
    }

    //------------------------------ Draw Food -------------------------------------//
    gl.bindBuffer( gl.ARRAY_BUFFER,bufferId);
    gl.bufferData( gl.ARRAY_BUFFER,flatten(food_vertices),gl.STATIC_DRAW);
    for(var i = 0; i < foods.length; ++i)
    {
        // draw color
        gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
        gl.bufferData( gl.ARRAY_BUFFER, flatten([foods[i].color,foods[i].color,foods[i].color,foods[i].color]), gl.STATIC_DRAW );

        gl.uniform2fv(locationLoc, vec2(foods[i].x/10, foods[i].y/10));
        gl.drawArrays(gl.TRIANGLE_FAN, 0, 3);
    }
    window.requestAnimFrame(render);
}
