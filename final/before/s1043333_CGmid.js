
var canvas;
var gl;
var bufferId;
var vPosition;
var cBuffer;
var normalBuffer;
// var colors = [
    // vec4( 0.0, 0.0, 1.0, 1.0 ),  // blue
    // vec4( 0.0, 1.0, 1.0, 1.0 ),  // cyan
    // vec4( 1.0, 0.0, 1.0, 1.0 ),  // magenta
    // vec4( 0.0, 1.0, 1.0, 1.0 )   // cyan
// ];
var colors = vec4( 0.0, 0.0, 1.0, 1.0 );
var vertices = [
    vec2( -0.05, -0.05 ),
    vec2( -0.05,  0.05 ),
    vec2(  0.05,  0.05 ),
    vec2(  0.05, -0.05 )
];

var  normals = [
    vec3(0,0,1),
    vec3(0,0,1),
    vec3(0,0,1),
    vec3(0,0,1)
];


var snake_bodies = [];
var xRotationRadians = 0,yRotationRadians = 0, zRotationRadians = 0;

var gl_modelViewMatrix;
var modelViewMatrix = mat4();



window.onload = function init()
{
    // for(var x = -2; x <3;++x)
    // {
    //     for(var y = -2; y <3;++y)
    //     {
    //         snake_bodies.push({x:x*1.2,y:y*1.2});
    //     }
    // }
    snake_bodies.push({x:0,y:0});
    canvas = document.getElementById( "gl-canvas" );
    
    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    //  Configure WebGL
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    //Do something ...
    /////////////////////////////////////////////////////////////////////////////////
    // Load the cube location data into the GPU
    var program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    bufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER,bufferId);
    gl.bufferData( gl.ARRAY_BUFFER,flatten(vertices),gl.STATIC_DRAW);

    // Associate out shader variables with our data buffer
    vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    // Color cube
    cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten([colors,colors,colors,colors]), gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );

    locationLoc = gl.getUniformLocation(program,"location");



    // Color cube
    normalBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, normalBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(normals), gl.STATIC_DRAW );

    var normalLocation = gl.getAttribLocation(program, "a_normal");
    gl.vertexAttribPointer( normalLocation, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( normalLocation );

    var reverseLightDirectionLocation =
        gl.getUniformLocation(program, "u_reverseLightDirection");
    // 设置光线方向
    gl.uniform3fv(reverseLightDirectionLocation, m4.normalize([0.5, 0.7, 1]));

    gl_modelViewMatrix =  gl.getUniformLocation(program, 'uModelViewMatrix');


    // Setup a ui.
    webglLessonsUI.setupSlider("#xRotation", {value: xRotationRadians, slide: updatexRotation, min: -360, max: 360});
    webglLessonsUI.setupSlider("#yRotation", {value: yRotationRadians, slide: updateyRotation, min: -360, max: 360});
    webglLessonsUI.setupSlider("#zRotation", {value: zRotationRadians, slide: updatezRotation, min: -360, max: 360});

    function updatexRotation(event, ui) {
        // console.log(modelViewMatrix);
        xRotationRadians = ui.value;
        modelViewMatrix = mult( rotate(  // matrix to rotate
            xRotationRadians,   // amount to rotate in radians
            [1, 0, 0]) , modelViewMatrix); // axis to rotate around

        // console.log(modelViewMatrix);
        render();
    }
    function updateyRotation(event, ui) {
        yRotationRadians = ui.value;
        modelViewMatrix = mult( rotate(  // matrix to rotate
            yRotationRadians,   // amount to rotate in radians
            [0, 1, 0]),modelViewMatrix); // axis to rotate around
        render();
    }
    function updatezRotation(event, ui) {
        // console.log(modelViewMatrix);
        zRotationRadians = ui.value;
        modelViewMatrix = mult(
            rotate(  // matrix to rotate
            zRotationRadians,   // amount to rotate in radians
            [0, 0, 1])
            , modelViewMatrix); // axis to rotate around
        // console.log(modelViewMatrix);
        render();
    }


    render();
};


function render() {
    gl.clear( gl.COLOR_BUFFER_BIT );


    //------------------------------ Draw Snake -------------------------------------//
    gl.bindBuffer( gl.ARRAY_BUFFER,bufferId);
    gl.bufferData( gl.ARRAY_BUFFER,flatten(vertices),gl.STATIC_DRAW);
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten([colors,colors,colors,colors]), gl.STATIC_DRAW );
    for(var i = 0; i < snake_bodies.length; ++i)
    {
        gl.uniform2fv(locationLoc, vec2(snake_bodies[i].x/10, snake_bodies[i].y/10));
        gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
    }
    //////////////////////////////////////////////////////////////////////////////////////////////
    // Set the drawing position to the "identity" point, which is
    // the center of the scene.


    // Now move the drawing position a bit to where we want to
    // start drawing the square.




    gl.uniformMatrix4fv(
        gl_modelViewMatrix,
        false,
        flatten(modelViewMatrix));

    //------------------------------ Draw Food -------------------------------------//
    // gl.bindBuffer( gl.ARRAY_BUFFER,bufferId);
    // gl.bufferData( gl.ARRAY_BUFFER,flatten(food_vertices),gl.STATIC_DRAW);
    // for(var i = 0; i < foods.length; ++i)
    // {
    //     // draw color
    //     gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    //     gl.bufferData( gl.ARRAY_BUFFER, flatten([foods[i].color,foods[i].color,foods[i].color,foods[i].color]), gl.STATIC_DRAW );
    //
    //     gl.uniform2fv(locationLoc, vec2(foods[i].x/10, foods[i].y/10));
    //     gl.drawArrays(gl.TRIANGLE_FAN, 0, 3);
    // }
    window.requestAnimFrame(render);
}

function radToDeg(r) {
    return r * 180 / Math.PI;
}

function degToRad(d) {
    return d * Math.PI / 180;
}
